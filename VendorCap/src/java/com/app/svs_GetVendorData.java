/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app;

import com.app.orientdb.OrientCRUDFPS;
import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class svs_GetVendorData {
  
    private final Gson JSON = new Gson();    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws ServletException, IOException {
    
        
        Map<String, Dto_VendorData> result = (Map<String, Dto_VendorData>) 
                                                db.getDbms().read("VendorData", Dto_VendorData.class);
       
        List<Dto_GetVendorData> res = new ArrayList<>(result.size());

    
        for (Map.Entry<String, Dto_VendorData> entry : result.entrySet()) {
            String string = entry.getKey();
            Dto_VendorData dto_VendorData = entry.getValue();
            
            res.add(new Dto_GetVendorData(string, dto_VendorData));
        }
                
        //response.getWriter().print(JSON.toJson(res.removeAll(res)));
        response.getWriter().print(JSON.toJson(res));
        //db.getDbms().delete("VendorData");
    }
    
}
