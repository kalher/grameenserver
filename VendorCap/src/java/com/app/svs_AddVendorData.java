/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app;

import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class svs_AddVendorData {
    
    private final Gson JSON = new Gson();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws ServletException, IOException {
    
        //get vendor data

        String msg = request.getParameter("msg");

        if (msg == null || msg.isEmpty()) {
            response.getWriter().print(JSON.toJson(new Msg("err_01", "Null msg parameter recieved")));
            return;
        }

        Dto_VendorData data = JSON.fromJson(msg, Dto_VendorData.class);

        //Add vendor

        db.getDbms().create("VendorData", data);

        response.getWriter().print(JSON.toJson(new Msg("succ_01", "Added Vendor data")));
    
    }
    
}
