/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app;

import com.app.orientdb.OrientFacade;
import com.app.cmdty.Svc_AddCommodity;
import com.app.cmdty.Svc_GetCmdtysPerVndr;
import com.app.mkt.Svc_AddMarket;
import com.app.submsn.Svc_AddSubmission;
import com.app.cmdty.Svc_GetCommodity;
import com.app.mkt.Svc_GetMarket;
import com.app.submsn.Svc_GetSubmission;
import com.app.submsn.Svc_Upd8Submission;
import com.app.vendor.Svc_GetVendor;
import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
@WebServlet(name = "Dispatch", urlPatterns = {"/*"})
public class Dispatch extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
        
    
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        OGlobalConfiguration.STORAGE_KEEP_OPEN.setValue(false);
    }
      
    @Override
    public void destroy() {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
        ODatabaseDocumentPool.global().close();
    }
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //url path
        String path = request.getPathInfo().substring(1);
        //db path
        String pathToDb = "C:\\Users\\hbt\\Documents\\NetBeansProjects\\VendorCap\\db";
        OrientFacade db = new OrientFacade(pathToDb, "admin", "admin");
        
        db.start();
        
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Cache-Control", "max-age=0");        
         
       System.out.println("###################\nURL : "+path+"\n#################"); 
        
        if(path.equals("add"))
        {
            svs_AddVendorData add = new svs_AddVendorData();        
            add.processRequest(request, response,db);        
        }
        else if(path.equals("upd8")){
            Svc_Upd8Submission upd8Submsn = new Svc_Upd8Submission();
            upd8Submsn.processRequest(request, response, db);
        }
        else if(path.equals("add/mkt"))
        {
            Svc_AddMarket addMkt = new Svc_AddMarket();
            addMkt.processRequest(request, response, db);
        }
        else if(path.equals("get/mkt"))
        {
            Svc_GetMarket getMkt = new Svc_GetMarket();
            getMkt.processRequest(request, response, db);
        }
        else if(path.equals("add/cmdty"))
        {
            Svc_AddCommodity addCmdty = new Svc_AddCommodity();
            addCmdty.processRequest(request, response, db);
        }
        else if(path.equals("get/cmdty"))
        {
            Svc_GetCommodity getCmdty = new Svc_GetCommodity();
            getCmdty.processRequest(request, response, db);
        }
        else if(path.equals("get/cmdty/vndr"))
        {
            Svc_GetCmdtysPerVndr getCPVndr = new Svc_GetCmdtysPerVndr();
            getCPVndr.processRequest(request, response, db);
        }        
        else if(path.equals("add/submsn"))
        {
            Svc_AddSubmission addSubmsn = new Svc_AddSubmission();
            addSubmsn.processRequest(request, response, db);            
        }
        else if(path.equals("get/submsn"))
        {
            Svc_GetSubmission getSubmsn = new Svc_GetSubmission();
            getSubmsn.processRequest(request, response, db);
        }
        else if(path.equals("get/vndr"))
        {
            Svc_GetVendor getVndr = new Svc_GetVendor();
            getVndr.processRequest(request, response, db);
        }        
        else if(path.equals("")){
            response.getWriter().print("No Url path specified");
        }
        else
        {
            response.getWriter().print("Bad request sent");
        }
            
        db.stop();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
