/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.submsn;

/**
 *
 * @author hbt
 */
public class Dto_AddSubmission {
    
    public String mkt, cmdty, price, unit, vendor;

    public Dto_AddSubmission(String mkt, String cmdty, String price, String unit, String vendor) {
        this.mkt = mkt;
        this.cmdty = cmdty;
        this.price = price;
        this.unit = unit;
        this.vendor = vendor;
    }

    public String getMkt() {
        return mkt;
    }

    public void setMkt(String mkt) {
        this.mkt = mkt;
    }

    public String getCmdty() {
        return cmdty;
    }

    public void setCmdty(String cmdty) {
        this.cmdty = cmdty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    
}
