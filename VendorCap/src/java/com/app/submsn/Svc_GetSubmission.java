/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.submsn;

import com.app.cmdty.Dto_GetCommodity;
import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_GetSubmission {
   
    private final Gson JSON= new Gson();
    
    public void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws IOException, ServletException{
    
    
    
    // get from db
        
        Map<String,Dto_AddSubmission> Table=(Map<String,Dto_AddSubmission>)db.getDbms().read("Submission", Dto_AddSubmission.class);
        
        //change map to list for ease
        List<Dto_GetSubmission> result= new ArrayList<>(Table.size());
        for (Map.Entry<String, Dto_AddSubmission> entry : Table.entrySet()) {
            String id = entry.getKey();
            Dto_AddSubmission submission = entry.getValue();
           result.add(new Dto_GetSubmission(id, submission));
        }
    response.getWriter().print(JSON.toJson(result));
        
        
    }
    
    
    
    
}
