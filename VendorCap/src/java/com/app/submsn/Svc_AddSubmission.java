/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.submsn;

import com.app.*;
import com.app.orientdb.OrientFacade;
import com.app.vendor.Dto_AddVendor;
import com.google.gson.Gson;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_AddSubmission {
    
    private final Gson JSON = new Gson();
    
    public void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws ServletException, IOException {
    
        //get vendor data

        String msg = request.getParameter("msg");

        if (msg == null || msg.isEmpty()) {
            response.getWriter().print(JSON.toJson(new Msg("err_01", "Null msg parameter recieved")));
            return;
        }

        Dto_VendorData data = JSON.fromJson(msg, Dto_VendorData.class);
                
        //Add vendor

        Map<String,Dto_AddVendor> vendors = (Map<String,Dto_AddVendor>) db.getDbms().search("Vendor", "name", data.getName(), Dto_AddVendor.class);
        

        Dto_AddSubmission submission = new Dto_AddSubmission(
                                                                data.getMarket(), 
                                                                data.getCommodity(), 
                                                                data.getPrice(), 
                                                                data.getUnit(), 
                                                                "");
                
        if( vendors.size() > 0 )
        {
            submission.setVendor(vendors.entrySet().iterator().next().getKey());
        }
        else
        {
            Dto_AddVendor vendorBuff = new Dto_AddVendor(data.getName());
            ODocument vendor = db.getDbms().create("Vendor", vendorBuff);
            submission.setVendor(vendor.field("@rid")+"");
        }
        
        db.getDbms().create("Submission", submission);
        
        response.getWriter().print(JSON.toJson(new Msg("succ_01", "Added Submission")));    
    }
    
}
