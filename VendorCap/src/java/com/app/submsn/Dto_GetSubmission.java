/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.submsn;

/**
 *
 * @author hbt
 */
public class Dto_GetSubmission {
    
    public String id;
    public Dto_AddSubmission submsn;

    public Dto_GetSubmission(String id, Dto_AddSubmission submsn) {
        this.id = id;
        this.submsn = submsn;
    }

    public Dto_GetSubmission() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Dto_AddSubmission getSubmsn() {
        return submsn;
    }

    public void setSubmsn(Dto_AddSubmission submsn) {
        this.submsn = submsn;
    }
    
    
    
}
