/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.submsn;

import com.app.*;
import com.app.orientdb.OrientFacade;
import com.app.vendor.Dto_AddVendor;
import com.google.gson.Gson;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_Upd8Submission {
    
    private final Gson JSON = new Gson();
    
    public void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws ServletException, IOException {
    
        //get vendor data

        String msg = request.getParameter("msg");

        if (msg == null || msg.isEmpty()) {
            response.getWriter().print(JSON.toJson(new Msg("err_01", "Null msg parameter recieved")));
            return;
        }

        Dto_GetVendorData data = JSON.fromJson(msg, Dto_GetVendorData.class);
        
        System.out.println("ID : " + data.getId() + "Price : " + data.getData().getPrice());
        
        db.getDbms().update(data.getId(), "price", data.getData().getPrice());
        db.getDbms().update(data.getId(), "unit", data.getData().getUnit());
                        
        response.getWriter().print(JSON.toJson(new Msg("succ_01", "Updated Submission")));    
    }
    
}
