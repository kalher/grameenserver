/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app;

import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class svs_SetPrices {
     private final Gson JSON = new Gson();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws ServletException, IOException {
    
            //get vendor data

        Dto_GetVendorData data = JSON.fromJson(request.getParameter("set"), Dto_GetVendorData.class);
            
        String name  =  data.getData().getName();
        String commod  =  data.getData().getCommodity();
        String market  = data.getData().getMarket();
        String newPrice = data.getData().getPrice();
        String unit = data.getData().getUnit();
        
        String id= data.getId();
        
            // check for vendor whose price is to be updated
        if(!name.equals("")){
            
             //Update vendor Data
            db.getDbms().update(id,"price" ,newPrice );
            response.getWriter().print("Data with updated price \n"+data);
                  
        }else{
            response.getWriter().print("No data Updated ");
        }
    }
}
