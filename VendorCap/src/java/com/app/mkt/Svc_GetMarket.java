/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.mkt;

import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_GetMarket {
   
    private final Gson JSON= new Gson();
    
    public void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws IOException, ServletException{
    
    
    
    // get from db
        
        Map<String,Dto_AddMarket> Table=(Map<String,Dto_AddMarket>)db.getDbms().read("Market", Dto_AddMarket.class);
        
        //change map to list for ease
        List<Dto_GetMarket> result= new ArrayList<>(Table.size());
        for (Map.Entry<String, Dto_AddMarket> entry : Table.entrySet()) {
            String id = entry.getKey();
            Dto_AddMarket market = entry.getValue();
           result.add(new Dto_GetMarket(id, market));
        }
    response.getWriter().print(JSON.toJson(result));
        
        
    }
    
    
    
    
}
