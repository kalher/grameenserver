/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.cmdty;

import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_GetCommodity {
   
    private final Gson JSON= new Gson();
    
    public void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws IOException, ServletException{
    
    
    
    // get from db
        
        Map<String,Dto_AddCommodity> Table=(Map<String,Dto_AddCommodity>)db.getDbms().read("Commodity", Dto_AddCommodity.class);
        
        //change map to list for ease
        List<Dto_GetCommodity> result= new ArrayList<>(Table.size());
        for (Map.Entry<String, Dto_AddCommodity> entry : Table.entrySet()) {
            String id = entry.getKey();
            Dto_AddCommodity commodity = entry.getValue();
           result.add(new Dto_GetCommodity(id, commodity));
        }
    response.getWriter().print(JSON.toJson(result));
        
        
    }
    
    
    
    
}
