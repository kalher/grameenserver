/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.vendor;

import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_AddVendor {
    
    private final Gson JSON=new Gson();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)throws IOException, ServletException{
    
    //get data from request
    Dto_AddVendor data=JSON.fromJson(request.getParameter("msg"), Dto_AddVendor.class);
    //add data to db
    db.getDbms().create("Vendor", data);
    }
}
