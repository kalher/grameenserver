/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.cmdty;

import com.app.Msg;
import com.app.mkt.Dto_AddMarket;
import com.app.mkt.Dto_GetMarket;
import com.app.orientdb.OrientFacade;
import com.app.submsn.Dto_AddSubmission;
import com.app.submsn.Dto_GetXSubmission;
import com.app.submsn.Dto_XSubmission;
import com.app.vendor.Dto_AddVendor;
import com.app.vendor.Dto_GetVendor;
import com.google.gson.Gson;
import java.io.IOException;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_GetCmdtysPerVndr {
   
    private final Gson JSON= new Gson();
    
    public void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)
            throws IOException, ServletException{
    
        // get from db        
        String msg = request.getParameter("msg");

        if (msg == null || msg.isEmpty()) {
            response.getWriter().print(JSON.toJson(new Msg("err_01", "Null msg parameter recieved")));
            return;
        }       
        
        Map<String,Dto_AddSubmission> submsns = (Map<String,Dto_AddSubmission>)db.getDbms().search("Submission", "vendor", msg, Dto_AddSubmission.class);
                
        //change map to list for ease
        List<Dto_GetXSubmission> result= new ArrayList<>(submsns.size());
        for (Map.Entry<String, Dto_AddSubmission> _subsmsn : submsns.entrySet()) {
            
            String id = _subsmsn.getKey();
            Dto_AddSubmission subsmsn = _subsmsn.getValue();
            
            Map<String , Dto_AddCommodity> _cmdty = (Map<String , Dto_AddCommodity>) db.getDbms().read(subsmsn.getCmdty(), "", Dto_AddCommodity.class);
            Dto_GetCommodity cmdty = new Dto_GetCommodity(subsmsn.getCmdty(),_cmdty.entrySet().iterator().next().getValue());
           
            Map<String,Dto_AddMarket > _mkt = (Map<String,Dto_AddMarket >)db.getDbms().read(subsmsn.getMkt(), "", Dto_AddMarket.class);
            Dto_GetMarket mkt = new Dto_GetMarket(subsmsn.getMkt(), _mkt.entrySet().iterator().next().getValue());
            
           Map<String,Dto_AddVendor>  _vndr = (Map<String,Dto_AddVendor>)db.getDbms().read(subsmsn.getVendor(), "", Dto_AddVendor.class);
            Dto_GetVendor vndr = new Dto_GetVendor(subsmsn.getVendor(), _vndr.entrySet().iterator().next().getValue());

            Dto_XSubmission _xsubmsn = new Dto_XSubmission(cmdty, mkt, vndr, subsmsn.getPrice(), subsmsn.getUnit());
            Dto_GetXSubmission xsubmsn = new Dto_GetXSubmission(id, _xsubmsn);
            result.add(xsubmsn);
        }
        
        response.getWriter().print(JSON.toJson(result));
        
    }
}
