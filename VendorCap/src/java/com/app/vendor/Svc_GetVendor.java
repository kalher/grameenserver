/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.vendor;

import com.app.orientdb.OrientFacade;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hbt
 */
public class Svc_GetVendor {
    private final Gson JSON=new Gson();
    public void processRequest(HttpServletRequest request, HttpServletResponse response, OrientFacade db)throws IOException, ServletException{
    
        //get from db
        
    //Map<String,Dto_AddVendor> Table=(<String,Dto_AddVendor>)db.getDbms().read("Vendor", Dto_AddVendor.class);
     Map<String,Dto_AddVendor> table=(Map<String, Dto_AddVendor>)db.getDbms().read("Vendor", Dto_AddVendor.class);
     //change map to list for ease
        List<Dto_GetVendor> result= new ArrayList<>(table.size());
        for (Map.Entry<String, Dto_AddVendor> entry : table.entrySet()) {
            String id = entry.getKey();
            Dto_AddVendor vendor = entry.getValue();
           result.add(new Dto_GetVendor(id,vendor));
    }

    response.getWriter().print(JSON.toJson(result));
    }
    
    
}