/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.app.orientdb;

import com.google.gson.Gson;
import com.orientechnologies.orient.core.command.OCommandRequest;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.exception.OCommandExecutionException;
import com.orientechnologies.orient.core.exception.OQueryParsingException;
import com.orientechnologies.orient.core.iterator.ORecordIteratorClass;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Bishaka
 */
public class OrientCRUDFPS {
    
    private final ODatabaseDocumentTx db;    
    private final Gson JSON = new Gson();
    
    public enum DOC_TYPE{
        CLUSTER,
        CLASS,
        DEFAULT
    };
    
    public enum LNK_TYPE{
        LINK
    }
    
    public enum REL_TYPE
    {
        REF,
        EMB,
        DEF
    }

    public OrientCRUDFPS(ODatabaseDocumentTx db) {
        this.db = db;
    }
    
    public ODatabaseDocumentTx getEngine()
    {
        return this.db;
    }
    
    public ODocument create(String name, Object content)
    {
        ODocument ref = new ODocument(name);
        ref.fromJSON(JSON.toJson(content));
        ref.save();
        return ref;
    }
        
    public List<ODocument> create(String name, List<Object> contents)
    {
        return create(name, contents.toArray(new Object[contents.size()]));
    }
    
    public List<ODocument> create(String name, Object[] contents)
    {
        List<ODocument> result = new ArrayList<>(contents.length);
        for( Object content : contents )
        {
            ODocument ref = new ODocument(name);
            ref.fromJSON(JSON.toJson(content));
            ref.save();        
            result.add(ref);
        }
        return result;
    }
        
    public Object create(String name, Object content, Class _class)
    {
        ODocument ref = new ODocument(name);
        
        Map<String,Object> result = new LinkedHashMap<String, Object>(1);
        ref.fromJSON(JSON.toJson(content));
        ref.save();
        
        String buff = ref.toJSON();
        result.put(ref.field("@rid")+"", JSON.fromJson(buff, _class));

        return result;
    }    

   /* 
        TODO :
        Only works with reference links, update to work with 
        embedded links as well
    */
    public void createLnk(String name, String _src, String _dest, REL_TYPE rel, String typ)
    {
        switch(rel)
        {
            case REF:
            case DEF:
                
                OClass src =( db.getMetadata().getSchema().existsClass(_src) )
                            ?
                            db.getMetadata().getSchema().getClass(_src)
                            : 
                            db.getMetadata().getSchema().createClass(_src);
                        
                        
                    
                OClass dest =( db.getMetadata().getSchema().existsClass(_dest) )
                            ?
                            db.getMetadata().getSchema().getClass(_dest)
                            : 
                            db.getMetadata().getSchema().createClass(_dest);

                src.createProperty(name, OType.valueOf(typ), dest);     

                                
            break;    
        }
    }    

    public void createLnk(String name, String _src, String _dest, String typ)
    {
        createLnk(name, _src, _dest, REL_TYPE.DEF, typ);
    }    
        
    public Object read(String name, Class _class)
    {
        return read(DOC_TYPE.DEFAULT, name, _class);
    }
    
    public Object read(DOC_TYPE typ, String name, Class _class)
    {

            Map<String,Object> result = new LinkedHashMap<String, Object>();

            switch(typ){

                case DEFAULT:
                case CLASS:
                default:   
                    try{
                            for(ODocument doc : db.browseClass(name))
                            {
                                String buff = doc.toJSON();
                                result.put( doc.field("@rid")+"", 
                                            JSON.fromJson(buff, _class)
                                           );
                            }
                    }catch(IllegalArgumentException iae){ /*no class found*/ }
                    
                return result;

                case CLUSTER:  
                    for(ODocument doc : db.browseCluster(name))
                    {
                        String buff = doc.toJSON();
                        result.put( doc.field("@rid")+"", 
                                    JSON.fromJson(buff, _class)
                                   );
                    }
                return result;
        }
    }
    
    public Object read(String id, String name, Class _class)
    {
        List<ODocument>	row = read(id);
        Map<String,Object> result = new LinkedHashMap<String, Object>
                                                            (row.size());
        ODocument doc = row.get(0);
        String buff = doc.toJSON();
        result.put(doc.field("@rid")+"", JSON.fromJson(buff, _class));

        return result;
    }

    public Object readAll(DOC_TYPE typ, String name)
    {                
        switch(typ){
            
            case DEFAULT:
            case CLASS:
            default:                       
            return db.browseClass(name);
                
            case CLUSTER:  
            return db.browseCluster(name);  
        }
    }
    
    public List<ODocument> read(String id)
    {
        return  db.query( new OSQLSynchQuery<ODocument>
                                                    ("select from "+id));
    }

    public List<ODocument> read(List<String> ids)
    {
        return read(ids.toArray(new String[ids.size()]));
    }
    
    public List<ODocument> read(String[] ids)
    {
        
        String id = "[";
        int count = 0;
        for (String _id : ids) {
            
            if(count > 0)
                id+=",";
            
            id+=_id;
            count++;
        }
        id+="]";
        
        return  db.query( new OSQLSynchQuery<ODocument>
                                                    ("select from "+id));
    }
    
    public Object read(List<String> ids, String name, Class _class)
    {
        return read(ids.toArray(new String[ids.size()]), name, _class);
    }

    public Object read(String[] ids, String name, Class _class)
    {

        List<ODocument>	row = read(ids);
        
        Map<String,Object> result = new LinkedHashMap<String, Object>
                                                            (row.size());
        
        for(ODocument doc : row)
        {
            String buff = doc.toJSON();
            result.put(doc.field("@rid")+"", JSON.fromJson(buff, _class));
        }        
        
        return result;
    }

    public void delete(String id)
    {
        read(id).get(0).delete();
    }
    
    public void delete(List<String> ids)
    {
        delete(ids.toArray(new String[ids.size()]));
    }
    
    public void delete(String[] ids)
    {
        for(ODocument doc : read(ids))
        {
            doc.delete();
        }
    }
    
    public void deleteAll(DOC_TYPE typ, String name)
    {
        switch(typ){
            
            case DEFAULT:
            case CLASS:
            default:                
                for(ODocument doc : db.browseClass(name))
                {
                    doc.delete();
                }
            break;
                
            case CLUSTER:  
                for(ODocument doc : db.browseCluster(name))
                {
                    doc.delete();
                }
            break;
                
        }        
    }
    
    public void update(String id, String field, Object value)
    {
        read(id).get(0).field(field,value).save();
    }
    
    //Only works with classes for now
    public boolean hasProperty(String name, String propName, String _type)
    {
        //Does the property exist?
        Boolean exists = db
                            .getMetadata()
                            .getSchema()
                            .getClass(name)
                            .existsProperty(propName);
        
        if(exists)
        {
            //Is it of the same type?
            String type = db
                            .getMetadata()
                            .getSchema()
                            .getClass(name)
                            .getProperty(propName)
                            .getType().toString();
            
            if( type.equalsIgnoreCase(_type) )
            {
                return true;
            }
        }
        
        return false;
        
    }
    
    public Object search(String name, String field, String value, Class _class)
    {
        
        List<ODocument> docs = new ArrayList<>();
        
        try{
        docs = db.query( new OSQLSynchQuery<ODocument>
                                        ("select from "
                                         + name
                                         + " where "
                                         + field
                                         + " like '"
                                         +value+"'"));  
        }
        catch(OCommandExecutionException ex){}
        catch(OQueryParsingException ex){}
        
        Map<String,Object> result = new LinkedHashMap<String, Object>
                                                            (docs.size());
        
        for(ODocument doc : docs)
        {
            String buff = doc.toJSON();
            result.put(doc.field("@rid")+"", JSON.fromJson(buff, _class));
        }        
        
        return result;        
    }
}
