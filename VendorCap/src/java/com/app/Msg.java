/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app;

/**
 *
 * @author hbt
 */
public class Msg {    
    String status, msg;

    public Msg() {
    }

    public Msg(String status, String msg) {
        this.status = status;
        this.msg = msg;
    }
    
    public Msg(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
     
}
