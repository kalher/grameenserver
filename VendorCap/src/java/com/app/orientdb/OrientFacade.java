/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.app.orientdb;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.exception.OSerializationException;
import com.orientechnologies.orient.core.exception.OStorageException;
import java.io.IOException;

/**
 *
 * @author Bishaka
 */
public class OrientFacade {
    
    private final String path, user, pass;
    private ODatabaseDocumentTx db;
    private OrientCRUDFPS dbms;

    public OrientFacade(String path, String user, String pass) {
        this.path = path;
        this.user = user;
        this.pass = pass;
    }

    public OrientCRUDFPS getDbms() {
        return dbms;
    }

    public void start()
    {

           
                
        try{
            
            db = ODatabaseDocumentPool
            .global()
            .acquire("plocal:"+path, user, pass);                                 
        }
        catch(OSerializationException ose)
        {
            
            ose.printStackTrace();
        }
        catch(OStorageException ose){
            //Db hasn't been created yet
            db = new ODatabaseDocumentTx("plocal:"+path)
                    .create();
        }       
        
        dbms = new OrientCRUDFPS(db);
        
    }
    
    public void stop()
    {
        try{}
        finally
        {
            db.close();
        }
        
    }
    
}
